from src.addition import addition

def test_addition():
    expected = 3
    actual = addition(1, 2)
    message = 'AssertionError! Expected "{}" != Actual "{}"'.format(expected, actual)
    assert expected == actual, message