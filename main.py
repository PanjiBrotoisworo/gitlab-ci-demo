from src.addition import addition
import numpy


def main():
	message = 'Hello world!'
	print(message)
	numpy_path = numpy.__file__
	print('Numpy path:', numpy_path)

	a = 1
	b = 2
	result = addition(a, b)
	print(f'{a} + {b} = {result}')

if __name__ == '__main__':
	main()